while :; do
	echo It will run forever unless you [CTRL+C] this

    printf 'first number: '
    read -r first

    printf 'operation [ + - * / ]: '
    read -r operation

    printf 'second number: '
    read -r second

    printf 'and the result is: '
    echo $(ruby calculator.rb -f$first -s$second -o$operation)
    echo
done    