# Letricia's calculator
require 'getoptlong'

opts = GetoptLong.new(
    ["--first-number", "-f", GetoptLong::REQUIRED_ARGUMENT],
    ["--second-number", "-s", GetoptLong::REQUIRED_ARGUMENT],
    ["--operation", "-o", GetoptLong::REQUIRED_ARGUMENT]
)

first_number = second_number = operation = nil

opts.each do |opt, arg|
  case opt
  when '--first-number'
    first_number = arg
  when '--second-number'
    second_number = arg
  when '--operation'
    operation = arg
  end
end

unless first_number.scan(/\D/).empty? && second_number.scan(/\D/).empty? &&
    %w{ + - * / }.include?(operation)
  raise ArgumentError, 'wrong argument(s)'
end

puts eval(first_number + operation + second_number)
