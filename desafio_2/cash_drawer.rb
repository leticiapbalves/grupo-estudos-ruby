class CashDrawer

  FIVE_BRL = 5.00
  TWO_BRL = 2.00
  ONE_BRL = 1.00
  FIFTY_CENTS = 0.50

  attr_reader :two_brl_total, :one_brl_total, :fifty_cents_total

  def initialize(two_brl_total, one_brl_total, fifty_cents_total)
    @five_brl_total = 0
    @two_brl_total = two_brl_total
    @one_brl_total = one_brl_total
    @fifty_cents_total = fifty_cents_total
  end

  def is_valid?(value)
    [FIVE_BRL, TWO_BRL, ONE_BRL, FIFTY_CENTS].include? value
  end

  def add_to_total(value, quantity = 1)
    case value
    when FIVE_BRL
      @five_brl_total = @five_brl_total + quantity
    when TWO_BRL
      @two_brl_total = @two_brl_total + quantity
    when ONE_BRL
      @one_brl_total = @one_brl_total + quantity
    else
      @fifty_cents_total = @fifty_cents_total + quantity
    end
  end

  def apply_change(change)
    @two_brl_total = @two_brl_total - change.two_brl_total
    @one_brl_total = @one_brl_total - change.one_brl_total
    @fifty_cents_total = @fifty_cents_total - change.fifty_cents_total
  end

  def get_info
    "%d x R$ %0.2f - %d x R$ %0.2f - %d x R$ %0.2f" %
        [@two_brl_total, TWO_BRL, @one_brl_total, ONE_BRL, @fifty_cents_total, FIFTY_CENTS]
  end

end