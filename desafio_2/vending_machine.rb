require_relative 'product_tray'
require_relative 'product'
require_relative 'cash_drawer'
require_relative 'vending_machine_ops'

items = []
items.push(ProductTray.new("02", Product.new("Diet Cola", 5.50), 1))
items.push(ProductTray.new("03", Product.new("Sprite", 5.50), 1))
items.push(ProductTray.new("01", Product.new("Cola", 5.50), 1))

vm = VendingMachineOps.new(items, CashDrawer.new(10, 5, 1))

loop do
  begin
    vm.show_items()
    vm.create_transaction()
    vm.receive_money()
    vm.drop_product()
    vm.give_change()
  rescue StandardError => e
    puts e.message
  end
  puts ""
end