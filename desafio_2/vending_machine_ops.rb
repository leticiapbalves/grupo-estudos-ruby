require_relative 'transaction'

class VendingMachineOps

  def initialize(items, cash_drawer)
    @items = items.sort_by { | item | item.code }
    @cash_drawer = cash_drawer
  end

  def show_items
    @items.find_all{ | item | item.quantity > 0 }
      .each { | item | puts item.get_info }
  end

  def create_transaction
    printf("Type the product code: ")
    product_code = gets.chomp

    item = @items.find{ | item | item.code == product_code }
    validate_item(item)

    printf("Type the amount to be inserted: ")
    total_amount = gets.chomp.to_f
    validate_amount(item, total_amount)

    @transaction = Transaction.new(item.product, total_amount)
    validate_change(@transaction.change)
  end

  def receive_money
    printf("Insert the bill / coin: ")
    value = gets.chomp.to_f

    begin
      validate_money(value)
      @transaction.add_to_current_amount!(value)
      @cash_drawer.add_to_total(value)
    rescue StandardError => e
      puts e.message
    end

    receive_money() if @transaction.current_amount < @transaction.total_amount
  end

  def drop_product
    @items.find{ | item | item.product == @transaction.product }.remove()
    puts "Dropping #{@transaction.product.name}..."
  end

  def give_change
    @cash_drawer.apply_change(@transaction.change)
    puts "Here's your change: #{@transaction.change.get_info}"
  end

  private

  def validate_item(item)
    raise ArgumentError, 'Sorry, item not available' if item.nil? || item.quantity == 0
  end

  def validate_amount(item, total_amount)
    raise ArgumentError, 'The amount is invalid' if item.product.price > total_amount
  end

  def validate_money(value)
    raise ArgumentError, 'The bill / coin is invalid' if !@cash_drawer.is_valid?(value)
  end

  def validate_change(change)
    raise ArgumentError, 'Sorry, no change for that' if change.two_brl_total > @cash_drawer.two_brl_total ||
        change.one_brl_total > @cash_drawer.one_brl_total ||
        change.fifty_cents_total > @cash_drawer.fifty_cents_total
  end
end