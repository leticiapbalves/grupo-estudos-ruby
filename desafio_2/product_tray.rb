class ProductTray

  attr_reader :code, :product, :quantity

  def initialize(code, product, quantity)
    @code = code
    @product = product
    @quantity = quantity
  end

  def remove
    @quantity = @quantity - 1
  end

  def get_info
    "#{@code} - #{@product.name}: R$ %0.2f" % [@product.price]
  end
end