require_relative 'cash_drawer'

class Transaction
  attr_reader :product, :total_amount, :current_amount, :change

  def initialize(product, total_amount)
    @product = product
    @total_amount = total_amount
    @change = calculate_change()
    @current_amount = 0.00
  end

  def add_to_current_amount!(value)
    @current_amount = @current_amount + value
  end

  private

  def calculate_change
    change_value = @total_amount - @product.price

    two_brl = change_value / CashDrawer::TWO_BRL
    change_value = change_value % CashDrawer::TWO_BRL

    one_brl = change_value / CashDrawer::ONE_BRL
    change_value = change_value % CashDrawer::ONE_BRL

    fifty_cents = change_value / CashDrawer::FIFTY_CENTS

    CashDrawer.new(two_brl, one_brl, fifty_cents)
  end
end